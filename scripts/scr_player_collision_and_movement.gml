// Movimiento horizontal (floor & air):

if (global.player_controlable)
{
    global.player_horiz_movement = global.player_pushed_btn_right - global.player_pushed_btn_left; // esto es para hacer flip al sprite
    
    if (global.player_actual_state != PLAYER_STATE.crouch && global.player_actual_state != PLAYER_STATE.crouch_attack  
        && global.player_actual_state != PLAYER_STATE.idle_attack 
        && (global.player_pushed_btn_left || global.player_pushed_btn_right ))
    {
        if (global.player_combo_run) //(global.player_actual_state == PLAYER_STATE.run || global.player_actual_state == PLAYER_STATE.jump_run)
            global.player_actual_x_speed = (global.player_pushed_btn_right - global.player_pushed_btn_left) * global.player_running_speed; 
        else
            global.player_actual_x_speed = (global.player_pushed_btn_right - global.player_pushed_btn_left) * global.player_walking_speed; 
        //show_message("  DENTRO;")
    }
    else { global.player_actual_x_speed = 0; }
}
else 
{
    if (global.player_looking_right) global.player_actual_x_speed = -1;
    else global.player_actual_x_speed = +1;
    
    global.player_actual_y_speed = global.player_actual_y_speed/3;
}

// show_message("  NOPE :(")
    
    
// Caer:
if (global.player_actual_y_speed < global.max_falling_speed)
{
    global.player_actual_y_speed += global.gravity;
}

// Saltar:
if (place_meeting(x, y + 1, obj_tile_normal))
{
    if (global.player_controlable && global.player_pushed_btn_jump)
    {
        global.player_actual_y_speed = -global.player_jumping_speed;
    }
}

// Disparar
if (global.player_controlable && global.player_pushed_btn_shot)
{
    var obj_projectil; // Declaramos una variable para asignarle un objeto proyectil
    correccion_x_proyectil = 6; // Ajusta (en píxeles) de dónde sale el proyectil en x, para que salga del cañón y no del centro
    correccion_y_proyectil = -1; // Ajusta (en píxeles) de dónde sale el proyectil en y
    
    if (global.player_actual_state == PLAYER_STATE.idle  || global.player_actual_state == PLAYER_STATE.walk  || 
        global.player_actual_state == PLAYER_STATE.idle_attack)  { correccion_x_proyectil = 6; correccion_y_proyectil = -7; }
    if (global.player_actual_state == PLAYER_STATE.crouch|| global.player_actual_state == PLAYER_STATE.crouch_attack){ correccion_x_proyectil = 6; correccion_y_proyectil = 5; }
    if (global.player_actual_state == PLAYER_STATE.run_attack)   { correccion_x_proyectil = 6; correccion_y_proyectil = -2; }
    if (global.player_actual_state == PLAYER_STATE.jump_attack)  { correccion_x_proyectil = 6; correccion_y_proyectil = -1; }
    
    if (global.player_looking_right) // Si el personaje mira a la derecha, se crea el proyectil y se le asigna velocidad a la derecha
    {
        obj_proyectil = instance_create(x+correccion_x_proyectil , y+correccion_y_proyectil, obj_player_shot);
        obj_proyectil.vel_horiz_bala  = global.player_shot_proj_speed;
    }
    else // De lo contrario (si mira a la izquierda), se crea el proyectil y se le asigna velocidad a la izquierda
    {
        obj_proyectil = instance_create(x-correccion_x_proyectil, y+correccion_y_proyectil, obj_player_shot);
        obj_proyectil.vel_horiz_bala  = -global.player_shot_proj_speed;
    }
    audio_play_sound(snd_player_shot, 10, false); // Se reproduce el sonido del disparo
    global.player_is_shooting = true; // El personaje está disparando (esto se usará para mostrar una animación u otra)
    global.player_shot_initial_moment = current_time; // Se inicia el contador al tiempo actual
    
    global.player_pushed_btn_shot = false; // Esto no me gusta, pero hay que ponerlo, parece :S
}
// Cuando transcurra el "tiempo_disparando" el personaje dejará de "estar disparando", afectando a su animación
if (global.player_is_shooting && (current_time - global.player_shot_initial_moment) >= global.player_shooting_time)
{
    global.player_is_shooting = false;
}






// Colisiones Horizontales --------------------------------------------------------------------------------------------------------------------
if (place_meeting(x + global.player_actual_x_speed, y, obj_tile_normal)) // Si la "x+vel_horiz" y la "y" del pers coinciden con la pos de un obj_wall...
{
    while(!place_meeting(x+sign(global.player_actual_x_speed), y, obj_tile_normal)) // ... mientras el personaje no esté a 1 píxel de colisionar...
    {
        x += sign(global.player_actual_x_speed); // ... se le suma o resta uno a su posición en "x".
    }
    global.player_actual_x_speed = 0; // ... y al acabar (es decir, cuando esté a 0 píxeles de distancia) se asigna 0 a la velocidad horizontal.
}


// Movemos en X lo que toque
x += global.player_actual_x_speed; // Tanto si se entra en el "if" como si no, se le suma la velocidad horizontal a la "x" (si entra en el "if", vel_hor será 0)
 


// Colisiones verticales ----------------------------------------------------------------------------------------------------------------------
if (place_meeting(x,y+global.player_actual_y_speed, obj_tile_normal)) // Si el personaje chocaría con el suelo/techo de seguir a la misma velocidad...
{
    while(!place_meeting(x, y+sign(global.player_actual_y_speed), obj_tile_normal)) // ... mientras el personaje no esté a 1 píxel de colisionar...
    {
        y += sign(global.player_actual_y_speed); // ... se le suma o resta uno a su posición en "y".
    }
    global.player_actual_y_speed = 0; // ... y asignamos 0 a la velocidad vertical.
}

// Movemos en Y lo que toque
y += global.player_actual_y_speed; // Se le suma la velocidad vertical a la y





/*
// Fin de fase ----------------------------------------------------------------------------------------------------------------------------------
AÑADIDO AL PROPIO OBJETO PUERTA

var margen_x = 16; // Margen en la x para entrar en "puerta de final de fase"
var margen_y = 16; // Margen en la y para entrar en "puerta de final de fase"
if instance_exists (obj_door) // Si existe el objeto obj_player...
{
    if (obj_player.x <= obj_door.x+ margen_x && obj_player.x >= obj_door.x - margen_x) // Si el personaje está dentro de los márgenes de "x"...
    {
        if (obj_player.y <= obj_door.y+ margen_y && obj_player.y >= obj_door.y - margen_y) //... y de los márgene de "y"...
        {   
            global.next_room = room_stg02;
            instance_create(0,0,obj_fade);
        }
    }
    with (obj_door)
    {
        show_debug_message("door x=" +string(x) + " y=" +string(y));
    }
    with (obj_player)
        show_debug_message("player x=" +string(x) + " y=" +string(y));
    
}
*/




// show_debug_message("global.player_actual_state = "+string(global.player_actual_state));
