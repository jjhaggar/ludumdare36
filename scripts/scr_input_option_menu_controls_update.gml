/// Option Menu - Input Update

var index = global.connected_controller;
xin_update(index); //Update the controller input - must be called before new input can be received.

if(keyboard_check_pressed(vk_escape) || (xin_check_pressed(index, xb_b)))   
{
    audio_play_sound(snd_menu_select, 10, false);
    global.game_state_actual = GAME_STATES.main_menu;
    instance_destroy();
}

if(keyboard_check_pressed(vk_enter) || (xin_check_pressed(index, xb_a)))   
{
    audio_play_sound(snd_menu_select, 10, false);
    
    switch(menuSelected) 
    {
        case 0: //DIFFICULTY
            global.difficulty = !global.difficulty;
        break;
        case 1: //FULLSCREEN
            show_debug_message("FullScreen");
            global.fullscreen = !global.fullscreen;
            window_set_fullscreen(global.fullscreen);
        break;
        case 2: //VOLUME
            show_debug_message("Volume");
            if (global.volume < 1) global.volume += 0.1;
            else global.volume = 0;
            audio_master_gain(global.volume);
        break;
        case 3: //AUTOSAVE
            global.auto_save = !global.auto_save ;
        break;
        case 4: //VIBRATION
            global.gamepad_vibration = !global.gamepad_vibration;
        break;
        case 5: // SAVE SETTINGS
            scr_ini_settings_save();
            global.game_state_actual = GAME_STATES.main_menu;
            instance_destroy();
        break;
    }
}


if(keyboard_check_pressed(vk_right) || (xin_check_pressed(index, xb_right)))   
{
    switch(menuSelected) 
    {
        case 2: //VOLUME
            if (global.volume < 1) global.volume += 0.1;
            else global.volume = 0;
            audio_master_gain(global.volume);
            audio_play_sound(snd_menu_select, 10, false);
        break;
    }
}


if(keyboard_check_pressed(vk_left) || (xin_check_pressed(index, xb_left)))   
{
    switch(menuSelected) 
    {
        case 2: //VOLUME
        
            if (global.volume > 0) global.volume -= 0.1;
            else global.volume = 1;
            audio_master_gain(global.volume);
            audio_play_sound(snd_menu_select, 10, false);
        break;
    }
}



show_debug_message(
"diff="+string(global.difficulty)+
"fullscr="+string(global.fullscreen)+
"vol="+string(global.volume)+
"autsave="+string(global.auto_save)+
"vibr="+string(global.gamepad_vibration)
)


if(keyboard_check_pressed(vk_up) || (xin_check_pressed(index, xb_up)))
{
    audio_play_sound(snd_menu_move, 10, false);
    menuSelected -=1;
    if (menuSelected < 0) { menuSelected = array_length_1d(menuText)-1; }
}

if(keyboard_check_pressed(vk_down) || (xin_check_pressed(index, xb_down)))
{
    audio_play_sound(snd_menu_move, 10, false);
    menuSelected += 1;
    if (menuSelected >= array_length_1d(menuText)) { menuSelected = 0; }
}


