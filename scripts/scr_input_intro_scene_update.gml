/// Option Menu - Input Update

var index = global.connected_controller;
xin_update(index); //Update the controller input - must be called before new input can be received.

if(keyboard_check_pressed(vk_anykey) 
    || (xin_check_pressed(index, xb_a ))
    || (xin_check_pressed(index, xb_b ))
    || (xin_check_pressed(index, xb_x ))
    || (xin_check_pressed(index, xb_y ))
    || (xin_check_pressed(index, xb_start ))
    )   
{
    audio_play_sound(snd_menu_select, 10, false);

    // Fade to ANY room
    global.next_room = room_stg01;
    instance_create(0,0,obj_fade);
    
    //instance_destroy();
}

