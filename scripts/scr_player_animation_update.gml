
// Dependiendo del estado actual se muestra una animación u otra
switch (global.player_actual_state)
{
    case PLAYER_STATE.idle:             sprite_index = spr_1p_standing;     break;
    case PLAYER_STATE.idle_attack:      sprite_index = spr_1p_s_attack;     break;
    case PLAYER_STATE.crouch:           sprite_index = spr_1p_crouching;    break;
    case PLAYER_STATE.crouch_attack:    sprite_index = spr_1p_c_attack;     break;
    case PLAYER_STATE.walk :            sprite_index = spr_1p_walking ;     break;
    case PLAYER_STATE.run:              sprite_index = spr_1p_running;      break;
    case PLAYER_STATE.run_attack :      sprite_index = spr_1p_r_attack;     break;
    case PLAYER_STATE.stopping:         sprite_index = spr_1p_standing;     break; //should be stopping
    case PLAYER_STATE.jump:             sprite_index = spr_1p_jumping;      break;
    case PLAYER_STATE.jump_attack:      sprite_index = spr_1p_j_attack;     break;
    case PLAYER_STATE.jump_run:         sprite_index = spr_1p_jumping;      break; // repetition
    case PLAYER_STATE.gethit:           sprite_index = spr_1p_gethit;       break;
    case PLAYER_STATE.dying:            sprite_index = spr_1p_dying;        break;
    
    case PLAYER_STATE.sp_hadoken:       sprite_index = spr_1p_sp_hadoken;   break;
    case PLAYER_STATE.sp_charge_a:      sprite_index = spr_1p_sp_charge_a;  break;
    case PLAYER_STATE.sp_charge_b:      sprite_index = spr_1p_sp_charge_b;  break;
    case PLAYER_STATE.sp_charge_c:      sprite_index = spr_1p_sp_charge_c;  break;
}

// Dependiendo de si se mueve el personaje horizontalmente, voltear sprite a un lado u otro
if (global.player_horiz_movement != 0) // Si el personaje se mueve horizontalmente (movimiento_horizontal puede ser 1 ó -1)...
{
    image_xscale = global.player_horiz_movement; // ... con 1 la imagen estaría normal y con -1 se voltearía horizontalmente
}

