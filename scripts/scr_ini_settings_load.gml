/// Load the INI settings

/*
Video FullScreen yes/no
Video Window Size #float [if player can resize manually calculate based on window height]
Volume Global 0/10
?Volume Musica 0/10
?Volume Sonidos 0/10
?Mute Global yes/no
?Mute Musica yes/no
?Mute Sonidos yes/no
AutoSave yes/no
Ctrl Keyboard Config [controles]
Ctrl JoyStick Config [controles]
*/

ini_open("game_settings.ini");
global.difficulty = ini_read_real("Settings", "Difficulty", 0);// 0=normal, 1=hard
global.fullscreen = ini_read_real("Settings", "FullScreen", 1); // 1 = true
global.window_size = ini_read_real("Settings", "WindowSize", 1); // use height to calculate width
global.volume  = ini_read_real("Settings", "Volume", 1); // use global.volume or audio_master_gain?
global.auto_save = ini_read_real("Settings", "AutoSave", 1);// 1 = true
// global.keyboard_jump = ini_read_string("Keyboard_Controls", "Jump", "vk_space"); // repeat
// global.joystick_jump = ini_read_string("Joystick_Controls", "Jump", "btn_01"); // repeat
global.gamepad_vibration = ini_read_real("Joystick_Controls", "Vibration", 1);// 1 = true
ini_close(); 

/*
// Example of ini.file ---------
[Settings]
FullScreen="false"
WindowSize="243.000000"
Volume="1.000000"
AutoSave="false"
[Keyboard_Controls]
Jump="vk_space"
[Joystick_Controls]
Jump="btn_01"
Vibration="true"
*/
