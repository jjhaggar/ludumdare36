/// Load the INI savegame XD

ini_open("game_save.ini");
global.IsThereASavedGame = ini_read_real("SaveGame", "IsThereASavedGame", 0); //0= false 1=true
global.PlayerName = ini_read_string("SaveGame", "PlayerName", "NoName");
global.ActualLevel = ini_read_real("SaveGame", "ActualLevel", 0);

ini_close(); 

