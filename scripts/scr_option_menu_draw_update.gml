/// Draw Menu

// This must be in a "draw event"

draw_sprite(spr_menu_options, image_index, 0, 0);


for (i = 0; i < array_length_1d(menuText); i++)
{
    if (menuSelected = i) { draw_set_color(col_menu_selected); } 
    else { draw_set_color(col_menu_not_selected); }
    
    
    
    switch(i) 
    {
        case 0: //DIFFICULTY
            if (global.difficulty)
                draw_text(x_main_menu, y_main_menu + i*distance_between_options, menuText[i] + " HARD");
            else 
                draw_text(x_main_menu, y_main_menu + i*distance_between_options, menuText[i] + " NORMAL");
        break;
        case 1: //FULLSCREEN
            if (global.fullscreen)
                draw_text(x_main_menu, y_main_menu + i*distance_between_options, menuText[i] + " FULL SCREEN");
            else
                draw_text(x_main_menu, y_main_menu + i*distance_between_options, menuText[i] + " WINDOWED");
        break;
        case 2: //VOLUME
            draw_text(x_main_menu, y_main_menu + i*distance_between_options, menuText[i] + " " + string_format(100 * global.volume, 0, 0) + "%");
        break;
        case 3: //AUTOSAVE
            if (global.auto_save)
                draw_text(x_main_menu, y_main_menu + i*distance_between_options, menuText[i] + " YES");
            else
                draw_text(x_main_menu, y_main_menu + i*distance_between_options, menuText[i] + " NO");
        break;
        case 4: //VIBRATION
            if (global.gamepad_vibration)
                draw_text(x_main_menu, y_main_menu + i*distance_between_options, menuText[i] + " YES");
            else
                draw_text(x_main_menu, y_main_menu + i*distance_between_options, menuText[i] + " NO");
        break;
        case 5: // SAVE SETTINGS
            draw_text(x_main_menu, y_main_menu + i*distance_between_options, menuText[i]);
        break;
    }
    
    
    
    
    
    
    
    // draw_text(x_main_menu, y_main_menu + i*distance_between_options, menuText[i]);
}

