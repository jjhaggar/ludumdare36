/// Main Menu - Input Update

var index = global.connected_controller;
xin_update(index); //Update the controller input - must be called before new input can be received.

if // global.can_change_to_next_room && 
(keyboard_check_pressed(vk_enter) || (xin_check_pressed(index, xb_a)))   
{
    audio_play_sound(snd_menu_select, 10, false);
    
    switch(menuSelected) 
    {
        case 0:
            // show_debug_message("start game");
            
            // Fade to room_intro
            global.next_room = room_intro;
            instance_create(0,0,obj_fade);
            
        break;
        case 1:
            
            //scr_ini_gamesaves_load();
            
            if (global.IsThereASavedGame){
                global.next_room = global.ActualLevel;
                instance_create(0,0,obj_fade);
            }
            else
            {
                show_debug_message("No hay ninguna partida salvada");
            }
            
        break;
        case 2:
            show_debug_message("Opciones");
            instance_create(0,0,obj_option_menu_handler);
            global.game_state_actual = GAME_STATES.options_menu;
            
        break;
        case 3:
            game_end();
        break;
    }
}

if(keyboard_check_pressed(vk_up) || (xin_check_pressed(index, xb_up)))
{
    audio_play_sound(snd_menu_move, 10, false);
    menuSelected -=1;
    if (menuSelected < 0) { menuSelected = array_length_1d(menuText)-1; }
}

if(keyboard_check_pressed(vk_down) || (xin_check_pressed(index, xb_down)))
{
    audio_play_sound(snd_menu_move, 10, false);
    menuSelected += 1;
    if (menuSelected >= array_length_1d(menuText)) { menuSelected = 0; }
}


