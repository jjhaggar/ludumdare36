//Loop to poll the state of all 4 controllers.

global.connected_controller = 0;

var index = 0; repeat(4)
{   
    //Update the controller input - must be called before new input can be received.
    if(xin_update(index)) 
    {
        //Function returned true, the specified controller is connected, and it's input.
        //state has been updated.
        show_debug_message("Controler " + string(index) + " is connected.")
        global.connected_controller = index;
        // exit;
    }
    else
    {
        show_debug_message("Controler " + string(index) + " is NOT connected.")
    }
    //Increment index so the loop checks the state of the next controller.
    index+=1;
}  

if (global.connected_controller != noone)
    show_debug_message("Main Controler is Controller " + string(global.connected_controller))
else 
    show_debug_message("Main Controler is NOONE");



