/// Controls updates

//show_debug_message("controlamos cosas");

switch (global.game_state_actual)
{
    case GAME_STATES.game:
    {
        //show_debug_message("JUEGO");
        // with (obj_main_menu_handler)
        scr_input_game_controls_update();
        break;
    }
    case GAME_STATES.main_menu:
    {
        with (obj_main_menu_handler)
            scr_input_main_menu_controls_update();
        break;
    }
    case GAME_STATES.options_menu:
    {
        with (obj_option_menu_handler)
            scr_input_option_menu_controls_update();        
        break;
    }
    case GAME_STATES.intro_scene:
    {
        with (obj_intro_scene)
            //show_debug_message("case GAME_STATES.intro_scene:")
            scr_input_intro_scene_update();
        break;
    }
    case GAME_STATES.pause_menu:
    {
        
        break;
    }
}
// show_debug_message(global.controller_buttonDisplay)
