/// UPDATE COMBO


// assign the values from global variables to local ones (with shorter names)
var comb_limit_time = global.player_combo_limit_time;
var comb_init_time = global.player_combo_init_time;
var comb_stored_mov = global.player_combo_stored_movements;
var btn = argument0;


// Si ha pasado el tiempo de combo, borramos el combo
if (current_time - comb_init_time > comb_limit_time){ comb_stored_mov = 0; }

// Last press -> Right
if (btn == "R"){
    if (comb_stored_mov == 0) { comb_init_time = current_time; }
    comb_stored_mov[array_length_1d(comb_stored_mov)] = "R";
    if (array_length_1d(comb_stored_mov) > 1)
    {
        if (comb_stored_mov[array_length_1d(comb_stored_mov)-2] == "R" && 
            comb_stored_mov[array_length_1d(comb_stored_mov)-1] == "R")
        {
            global.player_combo_run = true;
            show_debug_message("RUNNING! :D :D :D")
        }
    }
}


// Last press -> Left
if (btn == "L"){
    if (comb_stored_mov == 0) { comb_init_time = current_time; }
    comb_stored_mov[array_length_1d(comb_stored_mov)] = "L";
    if (array_length_1d(comb_stored_mov) > 1)
    {
        if (comb_stored_mov[array_length_1d(comb_stored_mov)-2] == "L" && 
            comb_stored_mov[array_length_1d(comb_stored_mov)-1] == "L")
        {
            global.player_combo_run = true;
            show_debug_message("RUNNING! :D :D :D")
        }
    }
}


// return values to global variables
global.player_combo_limit_time = comb_limit_time;
global.player_combo_init_time = comb_init_time;
global.player_combo_stored_movements = comb_stored_mov;

