// Colisión con enemigos ----------------------------------------------------------------------------------------------------------------------

if (global.player_vulnerable) // Si el personaje es vulnerable...
{
    //global.enemy_that_collides 
    var inst_enemigo = instance_place(x, y, obj_enemy); // Se intenta asignar una instancia de tipo obj_enemy que esté en colisión con el personaje
    if ((inst_enemigo != noone) && (inst_enemigo.e_is_dead == false)) // Si existe una instancia de enemigo que esté colisionando con el personaje...
    {    
        
        global.player_health  -= inst_enemigo.e_damage; // El personaje recibe el daño que haga el enemigo
        global.player_controlable = false; // El personaje deja de ser controlable 
        global.player_vulnerable = false; // El personaje deja de ser vulnerable (si no el contacto con un enemigo lo mataría siempre)
        
        if (global.player_health > 0) 
        { 
            audio_play_sound(snd_player_damage, 10, false); // Se reproduce el sonido de recibir daño
            alarm[0] = global.player_invulnerability_time_after_hit; // returns vulnerability
            alarm[1] = global.player_invulnerability_time_after_hit; // returns control
        }
        else // ha muerto
        {
            audio_play_sound(snd_player_death, 10, false); // Se reproduce el sonido de morir 
            alarm[0] = global.player_invulnerability_time_after_death; // returns vulnerability
            alarm[1] = global.player_invulnerability_time_after_death; // returns control
            
            
            
            if (global.player_lives > 0) // Si el personaje aún tiene vidas:
            {
                // room_restart();
                global.player_lives --; // Reducimos una "vida"
            }
            else // Si al personaje ya no le quedan vidas:
            {
                /*
                if show_question("GAME OVER") // Si se hace click en yes en la ventana con la pregunta:
                {
                    //global.puntos = 0;
                    //script_execute(script_room_ends, room_intro); // Se reinicia el juego desde la primera habitación (el número índice es 0)
                }
                else game_end(); // Si se hace click en no, se cierra el juego
                */
                show_message("GAME OVER");
                game_end();
            }   
            
            
            
            
            
            
            
            
            
            
        }
        
        
        return true;
    }
}
else 
{
    global.enemy_that_collides = noone;
    return false;
    
}
