//Update the controller input - must be called before new input can be received.
var index = global.connected_controller;

if(xin_update(index)) 
{
    //Function returned true, the specified controller is connected, and it's input.
    //state has been updated.
    
    //status[index]= "Connected.";
    
    //Get and store the state of controller inputs with values of variable intensity.
    
    //Triggers (Returns a value between 0 and 1)
    global.controller_leftTrigger = xin_get_state(index,xb_leftTrigger);
    rightTrigger[index] = xin_get_state(index,xb_rightTrigger);
    
    //(X,y) Position of the Left Thumbstick (Returns a value between -1 and 1)
    global.controller_leftThumbX = xin_get_state(index,xb_leftThumbX);
    global.controller_leftThumbY = xin_get_state(index,xb_leftThumbY);
    
    //(X,Y) Position of the Right Thumbstick (Returns a value between -1 and 1)
    global.controller_rightThumbX = xin_get_state(index,xb_rightThumbX);
    global.controller_rightThumbY = xin_get_state(index,xb_rightThumbY);
    
    //Set the controllers rumble intensity to that of the triggers.
    xin_set_rumble(index, global.controller_leftTrigger, global.controller_rightTrigger);
    
    //Prepare variables to poll currently pressed buttons.
    global.controller_buttonDisplay = "";
    global.controller_buttonInput = xin_get_input(index);
    
    while(buttonInput[index]) //While there is still input to be recieved.
    {
        switch(buttonInput[index]) //Switch the returned input.
        {
            case xb_a:              {global.controller_buttonDisplay += "#- 'A' Button"; break;}
            case xb_b:              {global.controller_buttonDisplay += "#- 'B' Button"; break;}
            case xb_x:              {global.controller_buttonDisplay += "#- 'X' Button"; break;}
            case xb_y:              {global.controller_buttonDisplay += "#- 'Y' Button"; break;}
            case xb_leftShoulder:   {global.controller_buttonDisplay += "#- Left Shoulder Button"; break;}
            case xb_rightShoulder:  {global.controller_buttonDisplay += "#- Right Shoulder Button"; break;}
            case xb_leftThumb:      {global.controller_buttonDisplay += "#- Left Thumbstick Button"; break;}
            case xb_rightThumb:     {global.controller_buttonDisplay += "#- Right Thumbstick Button"; break;}
            case xb_up:             {global.controller_buttonDisplay += "#- D-Pad Up Button"; break;}
            case xb_down:           {global.controller_buttonDisplay += "#- D-Pad Down Button"; break;}
            case xb_left:           {global.controller_buttonDisplay += "#- D-Pad Left Button"; break;}
            case xb_right:          {global.controller_buttonDisplay += "#- D-Pad Right Button"; break;}
            case xb_start:          {global.controller_buttonDisplay += "#- Start Button"; break;}
            case xb_back:           {global.controller_buttonDisplay += "#- Back Button"; break;}
        }
    
        buttonInput[index] = xin_get_input(index); //Check for more input.
    }
    
    //Version 1.1 supports check/pressed/released functions with the left and right triggers.
    //However the function xin_get_input() does not report trigger presses, so we must check these manually.
    if(xin_check(index,xb_leftTrigger)) {global.controller_buttonDisplay += "#- Left Trigger";}
    if(xin_check(index,xb_rightTrigger)) {global.controller_buttonDisplay += "#- Right Trigger";}
    
    //[Remember]: The intensity of trigger presses can be checked with the xin_get_state() function.
} 
else
{
    //'xin_update' returned false, generally indicating the specified controller is not connected.
    
    //Clear the display variables (Incase controller was disconnected during use).
    //status[index] = "Not Connected.";
    global.controller_leftTrigger = 0;
    global.controller_leftThumbX = 0;
    global.controller_leftThumbY = 0;
    global.controller_rightThumbX = 0;
    global.controller_rightThumbY = 0;
    global.controller_rightTrigger = 0;
    global.controller_buttonDisplay = "";
}

show_debug_message(global.controller_buttonDisplay)
