/// Draw Menu

// This must be in a "draw event"

draw_sprite(spr_menu_main, image_index, 0, 0);

for (i = 0; i < array_length_1d(menuText); i++)
{
    if (menuSelected = i) { draw_set_color(col_menu_selected); } 
    else { draw_set_color(col_menu_not_selected); }
    draw_text(x_main_menu, y_main_menu + i*distance_between_options, menuText[i]);
}

draw_set_color(c_white);
draw_text(40, 200, "USE Z X and Cursors or XBox360 GamePad");
