/// Audio Play Music


switch (argument0)

//switch (patata)
{
    case MUSICS.intro: // Music with intro and loop
    {
        global.music_playing = audio_play_sound(mus_dark_intro_start, 10, false); // Music intro
        global.music_to_play = mus_dark_intro_loop; // Music file with loop
        global.music_to_play_loops = true;
        alarm[0] = room_speed * 3.874; // -> music intro's length 
         show_debug_message("tras llamar alarma")
    }; 
    break;
 /*
    case MUSICS.intro_mal: // Music with loop (without intro)
    {
        global.music_playing = audio_play_sound(mus_dark_intro_loop , 10 , true); 
        break;
    }; 
    */
    case MUSICS.stage1: // Music with loop (without intro)
    {
        global.music_playing = audio_play_sound(mus_stage1_dust_loop, 10 , true); 
        break;
    }; 
    
    case MUSICS.stage2: // Music with loop (without intro)
    {
        global.music_playing = audio_play_sound(mus_stage2_temple_of_the_mystics, 10 , true); 
        break;
    }; 
    

    case MUSICS.boss: // Music with loop (without intro)
    {
        global.music_playing = audio_play_sound(mus_boss_inner_core, 10 , true); 
        break;
    }; 
    case MUSICS.ending_bad: // Music without loop
    {
        global.music_playing = audio_play_sound(mus_ending_bad_haloween, 10 , false); 
        break;
    }; 
   
    case MUSICS.ending_good: // Music without loop
    {
        global.music_playing = audio_play_sound(mus_ending_good_sad_piano, 10 , false); 
        break;
    }; 
    
}

