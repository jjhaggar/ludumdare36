/// Initialize menu


// menu_controlable = false;
// alarm[0] = room_speed * 0.25;

draw_set_font(global.fnt_bitmap_monospace);
draw_set_colour(c_white); draw_set_halign(fa_left); draw_set_valign(fa_top);

x_main_menu = 60;
y_main_menu = 36;

menuText[0] = "DIFFICULTY:" 
menuText[1] = "SCREEN:" 
menuText[2] = "VOLUME:"
menuText[3] = "AUTOSAVE:"
menuText[4] = "GAMEPAD VIBRATION:" 
menuText[5] = "SAVE SETTINGS"

menuSelected = 0; 

col_menu_selected = c_yellow;
col_menu_not_selected = c_white;

distance_between_options = 16;
