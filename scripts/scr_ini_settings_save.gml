/// Save the INI settings

ini_open("game_settings.ini"); //ini_open(string) where string is the file name (must be *.ini)
ini_write_real("Settings", "Difficulty", global.difficulty);// 0=normal, 1=hard
ini_write_real("Settings", "FullScreen", global.fullscreen);// 1 = true
ini_write_real("Settings", "WindowSize", global.window_size ); //ini_write_real(section,key,value)
ini_write_real("Settings", "Volume", global.volume ); //ini_write_real(section,key,value)
ini_write_real("Settings", "AutoSave", global.auto_save);// 1 = true
// ini_write_string("Keyboard_Controls", "Jump", global.keyboard_jump); // repeat
// ini_write_string("Joystick_Controls", "Jump", global.joystick_jump); // repeat
ini_write_real("Joystick_Controls", "Vibration", global.gamepad_vibration);// 1 = true
ini_close(); 

/*
// Example of ini.file -----------------------------------------------------------
[Settings]
FullScreen="false"
WindowSize="243.000000"
Volume="1.000000"
AutoSave="false"
[Keyboard_Controls]
Jump="vk_space"
[Joystick_Controls]
Jump="btn_01"
Vibration="true"
*/
