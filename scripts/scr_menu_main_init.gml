/// Initialize menu


// menu_controlable = false;
// alarm[0] = room_speed * 0.25;

draw_set_font(global.fnt_bitmap_monospace);
draw_set_colour(c_white); draw_set_halign(fa_left); draw_set_valign(fa_top);

x_main_menu = 180;
y_main_menu = 136;

menuText[0] = "START" 
menuText[1] = "CONTINUE"
menuText[2] = "OPTIONS"
menuText[3] = "EXIT"   

menuSelected = 0; 

col_menu_selected = c_yellow;
col_menu_not_selected = c_white;

distance_between_options = 16;
