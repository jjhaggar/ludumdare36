/// scr_check_lifes_and_continues

if (global.player_lives > 0) // Si el personaje aún tiene vidas:
{
    // room_restart();
    //global.player_lives --; // Reducimos una "vida"
    global.player_health = global.player_max_health; // Ponemos la vida/energía del personaje al máximo
    return true; 
}
else 
{
    obj_player.visible = false;
    
    if show_question("GAME OVER") // Si se hace click en yes en la ventana con la pregunta:
    {
        game_end();
        //global.puntos = 0;
        //script_execute(script_room_ends, room_intro); // Se reinicia el juego desde la primera habitación (el número índice es 0)
    }
    else game_end(); // Si se hace click en no, se cierra el juego
    
    return false;
}
