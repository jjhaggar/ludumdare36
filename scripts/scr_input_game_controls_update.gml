/// GAME - GET INPUTS 
// State machines https://youtu.be/-0bckaBj__M?t=8m10s


var index = global.connected_controller;
xin_update(index); //Update the controller input - must be called before new input can be received.

// TOGGLE PAUSE MENU
if(keyboard_check_pressed(vk_enter) || (xin_check_pressed(index, xb_start)))   
{
    with (obj_pause)
    {
        event_perform(ev_other, ev_user0);
        instance_activate_object(other);
    }
}

// EXIT GAME FROM PAUSE MENU
if (obj_pause.pause == true)
{
    if(keyboard_check_pressed(vk_escape))   
    {
        scr_ini_gamesaves_save();
        game_end();
    }
}









// CHECK BUTTON RELEASES -----------------------------------------------------------
if(keyboard_check_released(vk_left) || xin_check_released(index, xb_left))
{
    // global.player_looking_right = false; // necesario?
    global.player_pushed_btn_left = false;
    global.player_combo_run = false;    
}
if(keyboard_check_released(vk_right) || xin_check_released(index, xb_right))
{
    //global.player_looking_right = false; // necesario?
        global.player_pushed_btn_right = false;
        global.player_combo_run = false;
}
if(keyboard_check_released(vk_up) || xin_check_released(index, xb_up))
{
    // Personaje mira hacia arriba
}
if(keyboard_check_released(vk_down) || xin_check_released(index, xb_down))
{
    // Personaje mira hacia abajo
    global.player_pushed_btn_down = false;
}

if(keyboard_check_released(ord('Z')) || xin_check_released(index, xb_a)
|| keyboard_check_released(ord('C')) || xin_check_released(index, xb_y))
{
    // Personaje Salta
    global.player_pushed_btn_jump = false;
}

if(keyboard_check_released(ord('X')) || (xin_check_released(index, xb_x))
|| keyboard_check_released(vk_control) || (xin_check_released(index, xb_b)))
{
    // Personaje Dispara
    global.player_pushed_btn_shot = false;
}






// CHECK BUTTON PRESSES -----------------------------------------------------------
if(keyboard_check_pressed(vk_left) || xin_check_pressed(index, xb_left))
{
    // Personaje mira hacia la izquierda
    if (global.player_controlable) global.player_looking_right = false;
    global.player_pushed_btn_left = true;
    // global.player_horiz_movement = -1;
    scr_input_combo_update("L");
}
if(keyboard_check_pressed(vk_right) || xin_check_pressed(index, xb_right))
{
    // Personaje mira hacia abajo
    if (global.player_controlable) global.player_looking_right = true;
    global.player_pushed_btn_right = true;
    //show_message("    global.player_pushed_btn_right = true;")
    //global.player_horiz_movement = 1;
    
    scr_input_combo_update("R");
}

if(keyboard_check_pressed(vk_up) || xin_check_pressed(index, xb_up))
{
    // Personaje mira hacia arriba
    scr_input_combo_update("U");
}
if(keyboard_check_pressed(vk_down) || xin_check_pressed(index, xb_down))
{
    global.player_pushed_btn_down = true;
    scr_input_combo_update("D");
}
if(keyboard_check_pressed(ord('Z')) || xin_check_pressed(index, xb_a)
|| keyboard_check_pressed(ord('C')) || xin_check_pressed(index, xb_y))
{
    global.player_pushed_btn_jump = true;
    scr_input_combo_update("J");
}
if(keyboard_check_pressed(ord('X')) || (xin_check_pressed(index, xb_x))
|| keyboard_check_pressed(vk_control) || (xin_check_pressed(index, xb_b)))
{
    global.player_pushed_btn_shot = true;
    scr_input_combo_update("S");
}


