/// Game Init


// Declaring global.CONSTANTS (I still don't like GMS' Macros)
global.AUDIO_FADE = 0.2;
global.SCREEN_WIDTH = 384;
global.SCREEN_HEIGHT = 216;
global.DEFAULT_ZOOM = 1;


// Declaring enums
enum GAME_STATES
{
    main_menu,
    options_menu,
    intro_scene,
    game,
    pause_menu
    // Etc.
}

enum PLAYER_STATE
{
    idle,
    idle_attack,
    crouch,
    crouch_attack,
    walk,
    run,
    run_attack,
    stopping, // al frenar de una carrera?
    jump,
    jump_attack,
    jump_run, // este repite animación, pero ¿se comporta diferente? (inercia)
    gethit,
    dying,
    sp_charge_a,
    sp_charge_b,
    sp_charge_c,
    sp_hadoken
    // Etc.
}

// Declaring Global Variables


global.player_actual_state = PLAYER_STATE.idle;
global.player_looking_right = true;


global.can_change_to_next_room = false;

global.next_room = room_main_menu;
global.gravity = 0.9;
global.max_falling_speed = 10;

global.player_pushed_btn_jump = false;
global.player_pushed_btn_shot = false;
global.player_horiz_movement = 0; // Si está pulsada izq o dcha. +1 y -1
global.player_pushed_btn_down = false;
global.player_pushed_btn_right = false;
global.player_pushed_btn_left = false;
global.player_combo_limit_time = 300; // miliseconds
global.player_combo_init_time = false;
global.player_combo_stored_movements = 0; // array for storing combos
// global.player_combo_actual_time = false;

// global.player_combo_run_right = false;
// global.player_combo_run_left = false;
global.player_combo_run = false;
global.player_combo_hadoken = false;


global.player_actual_x_speed = 0; // Actual horizontal speed
global.player_actual_y_speed = 0;
global.player_walking_speed = 2;
global.player_running_speed = 6;
global.player_jumping_speed = 12;

global.player_max_health = 3; // ----
global.player_health = global.player_max_health; 
global.player_max_lives = 3; // -------
global.player_lives = global.player_max_lives; 
global.player_points = 0;
global.enemy_that_collides = noone;

global.player_shot_proj_speed = 10;
global.player_shot_proj_damage = 1;

global.player_shot_initial_moment = current_time; // borrar
global.player_shot_actual_moment = current_time; // borrar
global.player_shooting_time = 200; // steps
global.player_is_shooting = false;

global.player_hadoken_damage = 3;
global.player_charge_damage = 2;

global.player_controlable = true;
global.player_vulnerable = true;
global.player_invulnerability_time_after_hit = 15;
global.player_invulnerability_time_after_death = 30;

// Loading and applying INI (or default) settings
script_execute(scr_ini_settings_load);
window_set_fullscreen(global.fullscreen);
audio_master_gain(global.volume);

// Loading INI savegame (if it exists)
script_execute(scr_ini_gamesaves_load);

// Zooming window by default
window_set_size(global.SCREEN_WIDTH * global.DEFAULT_ZOOM, 
                global.SCREEN_HEIGHT * global.DEFAULT_ZOOM);

// INITIAL GAME STATE (maybe it should be on the initial code of the room)
global.game_state_actual = GAME_STATES.main_menu;

// Font initialization
global.fnt_bitmap_monospace = font_add_sprite(spr_bitmap_font, ord(" "), false, 0); // false is for monospace fonts

draw_set_font(global.fnt_bitmap_monospace);
draw_set_colour(c_white); draw_set_halign(fa_left); draw_set_valign(fa_top);





// Instantiating Handlers
instance_create(0, 0, obj_music_handler);
instance_create(0, 0, obj_input_handler);


// Going to 1st room

global.next_room = room_main_menu;
//global.next_room = room_intro;
//global.next_room = room_stg01;
 
room_goto(global.next_room );





// Tests
// with (obj_music_handler) scr_audio_play_music(  MUSICS.m03 ); // With is necessary to loop (WHY??)

//instance_create(0, 0, obj_hud_handler);




// script_execute(scr_ini_settings_save);
// script_execute(scr_savegame_save);


/*
instance_create(50, 50, obj_persistent_room_handler); // prueba persistencia
instance_create(50, 50, obj_player);
instance_create(100, 100, obj_rectangle);
instance_create(100, 100, obj_chocador);
instance_create(50, 50, obj_hijo); // prueba herencia
*/

//instance_create(50, 50, obj_nieto); // prueba herencia 2 gen --> NO FUNCIONA

// instance_create(0, 0, obj_view_modifier); // prueba view (cÃ¡mara)

// show_debug_message("hola!");




// show_debug_message("Â¡ADIOS!");



// ARRAY TESTS
/*    
a= 0;

show_debug_message("tamaño de un no array -> "+string(array_length_1d(a)));
    
a[0] = "5";
a[1] = "3";
a[2] = "2";
show_debug_message("tamaño de un array de 3 (0-2) -> "+string(array_length_1d(a)));


for (var i = array_length_1d(a) - 1; i > -1; i--;)
{
   show_debug_message("antes "+string(a[i]));
   a[i] = -1;
   show_debug_message("despues "+string(a[i]));
}

show_debug_message("tamaño Z -> "+string(array_length_1d(a)));
a= 0;
show_debug_message("tamaño 0 -> "+string(array_length_1d(a)));

a[0] = "5";
show_debug_message("tamaño ZZ -> "+string(array_length_1d(a)));
a[array_length_1d(a)] = 34;
show_debug_message("tamaño ZZZ -> "+string(array_length_1d(a)));
*/

