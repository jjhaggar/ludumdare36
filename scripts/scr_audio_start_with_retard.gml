/// This script should be used only on the "alarm 0" of obj_music_handler

show_debug_message("justo antes de sonar alarma")

global.mus_playing = audio_play_sound(global.music_to_play, 10, global.music_to_play_loops); 

show_debug_message("tras sonar alarma")


/* 
// How to use this:
    global.music_to_play = mus_NOMBREDELAMUSICA_loop; // Music file with loop
    global.mus_a_reproducir_loop = true;
    alarm[0] = room_speed * 3.474; // 3.474 -> music intro's length 
*/

