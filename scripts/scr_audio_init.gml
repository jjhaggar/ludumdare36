/// Audio Init

global.music_playing = noone; // Type music
global.music_to_play = noone; // Type resource
global.music_to_play_loops = true; // Bool true/false
// global.mus_to_play_priority = 10; // priority always 10 

enum MUSICS
{
    
    intro,
    //intro_mal,
    stage1,
    stage2,
    boss,
    ending_bad,
    ending_good
    // Etc.
}

// scr_audio_play_music( MUSICS.m03 );
