/// STATES
show_debug_message("global.player_actual_state = "+string(global.player_actual_state));
switch (global.player_actual_state)
{
    case PLAYER_STATE.idle: // IDLE 
    if (global.player_pushed_btn_down) global.player_actual_state = PLAYER_STATE.crouch;
    if ( !(global.player_pushed_btn_left && global.player_pushed_btn_right) && (global.player_pushed_btn_left || global.player_pushed_btn_right))
    {
        global.player_actual_state = PLAYER_STATE.walk;
    }
    if (global.player_combo_run){ global.player_actual_state = PLAYER_STATE.run;  }
    if (global.player_pushed_btn_jump) { global.player_actual_state = PLAYER_STATE.jump; } 
    if (global.player_actual_y_speed > 0){  global.player_actual_state = PLAYER_STATE.jump}
    if (global.player_is_shooting){ global.player_actual_state = PLAYER_STATE.idle_attack ; }
    
    if (scr_check_col_w_enemies()) 
    { 
        if (global.player_health > 0) { global.player_actual_state = PLAYER_STATE.gethit ;}
        else { global.player_actual_state = PLAYER_STATE.dying ;}
    }
    break;
    
    case PLAYER_STATE.walk: // WALK
    if (global.player_pushed_btn_down) global.player_actual_state = PLAYER_STATE.crouch;
    if (global.player_pushed_btn_left && global.player_pushed_btn_right || !global.player_pushed_btn_left && !global.player_pushed_btn_right)
        { global.player_actual_state = PLAYER_STATE.idle; }
    if (global.player_combo_run){ global.player_actual_state = PLAYER_STATE.run;  }
    if (global.player_pushed_btn_jump) { global.player_actual_state = PLAYER_STATE.jump; } 
    
    if (global.player_actual_y_speed > 0){  global.player_actual_state = PLAYER_STATE.jump}
    if (global.player_is_shooting){ global.player_actual_state = PLAYER_STATE.idle_attack ; } // si hubiera walking attack se usaría esa anim
    
    if (scr_check_col_w_enemies()) 
    { 
        if (global.player_health > 0) { global.player_actual_state = PLAYER_STATE.gethit ;}
        else { global.player_actual_state = PLAYER_STATE.dying ;}
    }
    break;

    case PLAYER_STATE.crouch: // CROUCH
    if (!global.player_pushed_btn_down) { global.player_actual_state = PLAYER_STATE.idle; }
    if (global.player_pushed_btn_jump) { global.player_actual_state = PLAYER_STATE.jump; } 
    if (global.player_is_shooting){ global.player_actual_state = PLAYER_STATE.crouch_attack ; }
    if (scr_check_col_w_enemies()) 
    { 
        if (global.player_health > 0) { global.player_actual_state = PLAYER_STATE.gethit ;}
        else { global.player_actual_state = PLAYER_STATE.dying ;}
    }
    break;
    
    case PLAYER_STATE.jump: // JUMP
    if (place_meeting(x, y + 1, obj_tile_normal)) { global.player_actual_state = PLAYER_STATE.idle; }
    if (global.player_is_shooting){ global.player_actual_state = PLAYER_STATE.jump_attack ; }
    if (scr_check_col_w_enemies()) 
    { 
        if (global.player_health > 0) { global.player_actual_state = PLAYER_STATE.gethit ;}
        else { global.player_actual_state = PLAYER_STATE.dying ;}
    }
    break;

    case PLAYER_STATE.run: // RUN
    if (!global.player_pushed_btn_left && !global.player_pushed_btn_right) ||
       (global.player_pushed_btn_left && global.player_pushed_btn_right)
    {
        global.player_combo_run = false;
    }
    if (!global.player_combo_run)
        global.player_actual_state = PLAYER_STATE.idle;
    
    if (global.player_pushed_btn_jump) { global.player_actual_state = PLAYER_STATE.jump_run; } 
    if (global.player_pushed_btn_down) global.player_actual_state = PLAYER_STATE.crouch;
    if (global.player_actual_y_speed > 0){  global.player_actual_state = PLAYER_STATE.jump} //no muy seguro de esto
    if (global.player_is_shooting){ global.player_actual_state = PLAYER_STATE.run_attack ; }
    if (scr_check_col_w_enemies()) 
    { 
        if (global.player_health > 0) { global.player_actual_state = PLAYER_STATE.gethit ;}
        else { global.player_actual_state = PLAYER_STATE.dying ;}
    }
    break;
    
    case PLAYER_STATE.jump_run: // JUMP RUN
    if (place_meeting(x, y + 1, obj_tile_normal)) 
    { 
        if (global.player_combo_run) { global.player_actual_state = PLAYER_STATE.run; }
        else { global.player_actual_state = PLAYER_STATE.walk; }
    }
    if (global.player_is_shooting){ global.player_actual_state = PLAYER_STATE.jump_attack ; }
    if (scr_check_col_w_enemies()) 
    { 
        if (global.player_health > 0) { global.player_actual_state = PLAYER_STATE.gethit ;}
        else { global.player_actual_state = PLAYER_STATE.dying ;}
    }
    break;
    
    case PLAYER_STATE.idle_attack:    
    if (!global.player_is_shooting) { global.player_actual_state = PLAYER_STATE.idle; }   
    if (global.player_pushed_btn_jump) { global.player_actual_state = PLAYER_STATE.jump_attack; } 
    if (global.player_pushed_btn_down) { global.player_actual_state = PLAYER_STATE.crouch_attack; } 
    if (scr_check_col_w_enemies()) 
    { 
        if (global.player_health > 0) { global.player_actual_state = PLAYER_STATE.gethit ;}
        else { global.player_actual_state = PLAYER_STATE.dying ;}
    }
    break;

    case PLAYER_STATE.crouch_attack:    
    if (!global.player_is_shooting) { global.player_actual_state = PLAYER_STATE.crouch; }   
    if (global.player_pushed_btn_jump) { global.player_actual_state = PLAYER_STATE.jump_attack; } 
    if (!global.player_pushed_btn_down) { global.player_actual_state = PLAYER_STATE.idle_attack; } 

    if (scr_check_col_w_enemies()) 
    { 
        if (global.player_health > 0) { global.player_actual_state = PLAYER_STATE.gethit ;}
        else { global.player_actual_state = PLAYER_STATE.dying ;}
    }
    break;

    case PLAYER_STATE.run_attack:    
    if (!global.player_pushed_btn_left && !global.player_pushed_btn_right) ||
       (global.player_pushed_btn_left && global.player_pushed_btn_right)
    {
        global.player_combo_run = false;
    }
    if (!global.player_combo_run)
        global.player_actual_state = PLAYER_STATE.idle_attack;
    if (global.player_combo_run && global.player_pushed_btn_jump) { global.player_actual_state = PLAYER_STATE.jump_attack; } 
    if (!global.player_is_shooting) { global.player_actual_state = PLAYER_STATE.run; }        
    if (scr_check_col_w_enemies()) 
    { 
        if (global.player_health > 0) { global.player_actual_state = PLAYER_STATE.gethit ;}
        else { global.player_actual_state = PLAYER_STATE.dying ;}
    }
    break;

    case PLAYER_STATE.jump_attack:    
    if (place_meeting(x, y + 1, obj_tile_normal)) 
    { 
        if (global.player_combo_run) { global.player_actual_state = PLAYER_STATE.run_attack; }
        else { global.player_actual_state = PLAYER_STATE.idle_attack; } // este debería ser walk attack si existiera
    }    
    if (!global.player_is_shooting) { global.player_actual_state = PLAYER_STATE.jump; }        
    if (scr_check_col_w_enemies()) 
    { 
        if (global.player_health > 0) { global.player_actual_state = PLAYER_STATE.gethit ;}
        else { global.player_actual_state = PLAYER_STATE.dying ;}
    }
    break;

    case PLAYER_STATE.gethit:    
    if (global.player_controlable) { global.player_actual_state = PLAYER_STATE.jump; }
    break;
    
    case PLAYER_STATE.dying:    
    if (global.player_controlable) 
    { 
        if (scr_check_lifes())
        {
            global.player_actual_state = PLAYER_STATE.jump; 
        }
        
    }
    break;

}
