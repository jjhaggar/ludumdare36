menu_controlable = false;
alarm[0] = room_speed * 0.25;



// Creamos y aplicamos la fuente
fnt_bitmap_font = font_add_sprite(spr_bitmap_font, ord(" "), false, 0); // false es para que sea monospace
draw_set_font(fnt_bitmap_font);
draw_set_colour(c_white); // Color fuente
draw_set_halign(fa_left); // AlineaciÃ³n horizontal de fuente: a la izquierda
draw_set_valign(fa_top);  // AlineaciÃ³n vertical de fuente: arriba 



// Preparamos el MenÃº Principal
x_main_menu = 180; // PosiciÃ³n en x delmenÃº
y_main_menu = 136; // PosiciÃ³n en y delmenÃº

menuText[0] = "Empezar"  // Texto de la opciÃ³n 0 (almacenamos los valores en un "array" para poder iterar entre ellos)
menuText[1] = "Continuar"     // Texto de la opciÃ³n 1
menuText[2] = "Instrucciones"// Texto de la opciÃ³n 2
menuText[3] = "Quitar"   // Texto de la opciÃ³n 3

menuSelected = 0; // OpciÃ³n seleccionada
